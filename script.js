class Employee{
    constructor(name, age, salary){
        this.name = name
        this.age = age
        this._salary = salary

        
    }
    
    get name(){
        return this._name
    }

    set name(value){
        if (value.length == 0 ){
            alert("Name cannot be empty")
            return;
        }
        this._name=value;
    }

    get age(){
        return this._age
    }

    set age(value){
        if (value.length == 0 ){
            alert("Age cannot be empty")
            return;
        }
        this._age=value;
    }

    get salary(){
        return this._salary
    }

    set salary(value){
        if (value.length <= 2 ){
            alert("Salary cannot be empty")
            return;
        }
        this._salary=value;
    }
}

class Programmer extends Employee{
    constructor(name, age, salary,lang){
        super(name, age, salary)
        this.lang = lang
    }

    get salary(){
        return this._salary * 3;
    }
}

let ProgrammerOne = new Programmer("Anton","27","1000","Java");
let ProgrammerTwo = new Programmer("Sasha","23","1200","C++");
let ProgrammerThree = new Programmer("Igor","30","1300","Python");

console.log(ProgrammerOne)
console.log(ProgrammerTwo)
console.log(ProgrammerThree)